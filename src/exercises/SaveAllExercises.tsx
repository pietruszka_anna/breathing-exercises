// @ts-ignore
import React, {useState} from "react";
import {Button} from "semantic-ui-react";
import {Exercise} from "../api/Exercise";

const SaveAllExercises: React.FunctionComponent = () => {
    const [exerciseInstance, setExerciseInstance] = useState<Exercise[]>([])
    const onSubmit = async () => {}

    return(
        <div>
            <Button onClick={onSubmit}>
              Dodaj ćwiczenia
            </Button>
        </div>
    );
}