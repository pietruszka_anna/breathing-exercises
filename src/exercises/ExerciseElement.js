import {Button, Card, Icon} from "semantic-ui-react";
import TagElement from "./TagElement";
import StepElement from "./StepElement";

export default function ExerciseElement(props) {
    return (
        <Card key={props.index}>
            <Card.Content header={props.name}/>
            <Card.Content description={props.description}/>
            <div style={{display: 'flex', alignItems: 'center'}}>
              <Card.Group>
                  {props.tags != null ? props.tags.map((tag, index) => (
                      <TagElement name={tag}/>
                  )) : null}
              </Card.Group>
            </div>
            <div style={{padding: "0 1rem 1rem 1rem"}}>
                <p><b>Serie:</b> {props.numberOfSeries}</p>
                <p><b>Kroki:</b></p>
                <Card.Group>
                    {props.steps !== null ? props.steps.map((step, index) => (
                        <StepElement index={index} action={step.action} time={step.time}/>
                    )) : null}
                </Card.Group>
            </div>
            <div style={{display: "flex", flexDirection: "row", justifyContent: "center"}}>
                <Button onClick={props.deleteExerciseFunction.bind(this, props.index, props.index)}>
                    <Icon name='cancel'/>
                </Button>
                <Button onClick={props.editFunction.bind(this, props.index, {
                    name: props.name,
                    description: props.description,
                    numberOfSeries: props.numberOfSeries,
                    steps: props.steps,
                    tags: props.tags,
                })}>
                    <Icon name='edit'/>
                </Button>
            </div>
        </Card>
    )
}