import styled from "styled-components";
import { Card, } from "semantic-ui-react";
import StepElement from "./StepElement";

export const CardContainer = styled.div`
display: flex;
  align-items: center;
  justify-content: center;
  border-radius: .5em;
  overflow: 'hidden';
box-shadow: -1px 0px 20px -10px rgba(137, 175, 201, 1);
`

export default function ExerciseStepsContainer(props) {
    return (
        <CardContainer>
            <div style={{padding: '2rem 2rem 2rem 2rem'}}>
                <Card.Group>
                    {props.questionList !== null ? props.questionList.map((q, index) => (
                        <StepElement action={q.action} time={q.time} deleteFunction={props.deleteFunction}
                                     index={index}/>
                    )) : null}
                </Card.Group>
            </div>
        </CardContainer>
    )
}