export default function TagElement(props) {
    return (
        <div style={{padding: '0.5rem', margin: '0.5rem', justifyContent: 'center', alignItems: 'center', backgroundColor: 'aliceblue',  borderRadius: '10px'}}>
            #{props.name}
        </div>
    )
}
