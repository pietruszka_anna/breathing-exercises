import {Button, Icon} from "semantic-ui-react";

export default function StepElement(props) {
    return (
        <div key={props.index} style={{margin: '5px', padding: '5px', backgroundColor:'lavender', borderRadius: '10px', display: 'flex'}}>
            <p><b>{props.index+1}.</b></p>
            <div style={{width: '10px'}}/>
            {props.action}
            <div style={{width: '10px'}}/>
            {props.time}s
            <div style={{width: '10px'}}/>
            {props.deleteFunction ? <Button size={"tiny"} onClick={props.deleteFunction.bind(this, props.index, props.index)}>
                <Icon onClick={props.deleteFunction.bind(this, props.index, props.index)} name='cancel'/>
            </Button> : <></>}
        </div>
    )
}