export interface Exercise {
    name: string,
    description: string,
    numberOfSeries: number,
    steps: Step[],
}

export interface Step {
    "action": "INHALE",
    "time": 2,
    "measurementParameter": MeasurementParameter,
}

export interface MeasurementParameter {
    parameter: ParameterType,
    minValue: number
}

enum ParameterType {
    PIF,
    PEF,
    VOLUME,
}
