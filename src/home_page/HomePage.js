import React, {useState} from 'react'
import {Button, Grid, Tab} from "semantic-ui-react";
import HomePageForm from "./components/HomePageForm";
import HomePageExercisesList from "./components/HomePageExercisesList";
import styled from "styled-components";
import {Upload} from "./components/UploadFile";


const HeaderContainer = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row;
  justify-content: end;
`

export default function HomePage(props) {

    const [exercises, setExercises] = useState({
        exercises: []
    });

    const [activeExercise, setActiveExercise] = useState({
        name: '',
        description: '',
        numberOfSeries: 0,
        tags: [],
        steps: [],
    })

    const deleteExerciseFunction = (event, index) => {
        var exercisesList = exercises.exercises
        exercisesList = exercisesList.filter((exercise, idx) => index !== idx);
        setExercises(prevState => ({
            ...prevState,
            exercises: exercisesList
        }));
    }

    const editFunction = (event, index, data) => {
        console.log(index)
        setActiveExercise(index)
    }


    const downloadFile = ({data, fileName, fileType}) => {
        const blob = new Blob([data], {type: fileType})
        const a = document.createElement('a')
        a.download = fileName
        a.href = window.URL.createObjectURL(blob)
        const clickEvt = new MouseEvent('click', {
            view: window,
            bubbles: true,
            cancelable: true,
        })
        a.dispatchEvent(clickEvt)
        a.remove()
    }

    const exportToJson = e => {
        e.preventDefault()
        downloadFile({
            data: JSON.stringify(exercises),
            fileName: 'exercises.json',
            fileType: 'text/json',
        })
    }

    const panes = [
        {
            menuItem: "Dodaj ćwiczenie oddechowe",
            render: () => <HomePageForm exercises={exercises} loadExercises={setExercises}
                                        activeExercise={activeExercise}/>,
        },
        {
            menuItem: "Dodane ćwiczenia oddechowe",
            render: () => <HomePageExercisesList exercises={exercises}
                                                 deleteExerciseFunction={deleteExerciseFunction}
                                                 editFunction={editFunction}/>,
        }
    ]
    return (
        <div>
            <HeaderContainer>
                <Grid.Row>
                    <Upload loadExercises={setExercises}/>
                    <Button onClick={exportToJson} color={"green"}>
                        Zapisz do pliku
                    </Button>
                </Grid.Row>
            </HeaderContainer>
            <Grid.Column>
                <Tab style={{padding: '3rem 0 0 0'}} menu={{
                    secondary: true, pointing: true, display: 'flex',
                    justify: 'center',
                    align: 'center',
                    color: "blue"
                }} panes={panes}/>
            </Grid.Column>
        </div>

    )
}
