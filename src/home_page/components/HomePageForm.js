import {Button, Card, Dropdown, Form, Grid, Input, Select} from "semantic-ui-react";
import {useState} from "react";
import styled from "styled-components";
import TagElement from "../../exercises/TagElement";
import ExerciseStepsContainer from "../../exercises/ExerciseStepsContainer";

const HomePageInnerContainer = styled.div`
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`

const HomePageForm = (props) => {
    const [tag, setTag] = useState("")

    const [userInput, setUserInput] = useState(props.activeExercise);

    const [step, setStep] = useState({
        action: '',
        time: '',
        measurementParameter: '',
    })

    const handleExerciseNameChange = (e, data) => {
        setUserInput({
            ...userInput,
            name: data.value
        })
    }

    const handleTagChange = (e, data) => {
        setTag(data.value)
    }

    const handleExerciseDescriptionChange = (e, data) => {
        setUserInput((prevState) => {
            return {
                ...prevState,
                description: data.value
            }
        })
    }

    const handleNumberOfSeriesChange = (e, data) => {
        setUserInput((prevState) => {
            return {
                ...prevState,
                numberOfSeries: data.value
            }
        })
    }

    const handleExerciseStepsChange = (e, data) => {
        e.preventDefault();
        var newArr = userInput.steps;
        newArr.push({
            action: step.action,
            time: step.time,
            measurementParameter: step.measurementParameter,
        });
        setUserInput((prevState) => {
            return {
                ...prevState,
                steps: newArr
            }
        })
        setStep((prevState => {
            return {
                ...prevState,
                time: '',
                measurementParameter: ''
            }
        }))
        e.preventDefault();
    }

    const handleHashtagsChange = (e, data) => {
        var newArr = userInput.tags;
        newArr.push(tag)
        setUserInput((prevState) => {
            return {
                ...prevState,
                tags: newArr
            }
        })
        setTag("")
    }

    const handleStepActionChange = (e, data) => {
        setStep((prevState) => {
            return {
                ...prevState,
                action: data.value
            }
        })
    }

    const handleStepTimeChange = (e, data) => {
        setStep((prevState) => {
            return {
                ...prevState,
                time: data.value
            }
        })
    }

    const handleMeasurementParameterTypeChange = (e, data) => {
        setStep((prevState) => {
            return {
                ...prevState,
                measurementParameter: data.value
            }
        })
    }

    const handleAddExercise = (event) => {
        event.preventDefault();

        const exercise = {
            name: userInput.name,
            description: userInput.description,
            numberOfSeries: userInput.numberOfSeries,
            steps: userInput.steps,
            tags: userInput.tags,
        }
        var exercisesList = props.exercises.exercises;
        exercisesList.push(exercise)
        props.loadExercises(prevState => ({
            ...prevState,
            exercises: exercisesList
        }));
        setUserInput({
            name: '',
            description: '',
            numberOfSeries: 0,
            steps: [],
            tags: [],
        })
        event.preventDefault();
    }

    const deleteFunction = (event, index) => {
        var stepList = userInput.steps
        stepList = stepList.filter((step, idx) => (index!== idx))
        setUserInput(prevState => ({
            ...prevState,
            steps: stepList
        }));
    }

    const questionTypeOptions = [
        {
            text: 'WDECH',
            value: 'WDECH'
        }, {
            text: 'WYDECH',
            value: 'WYDECH'
        }, {
            text: 'PAUZA',
            value: 'PAUZA'
        }]

    const parameterOptions = [
        {
            text: 'OBJETOSC',
            value: 'OBJETOSC'
        }, {
            text: 'PEF',
            value: 'PEF'
        }, {
            text: 'PIF',
            value: 'PIF'
        }]

    return (<HomePageInnerContainer>
            <div>
                <Grid as={Form}>
                    <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                        <Grid.Column>
                            <h4>Nazwa ćwiczenia</h4>
                            <Form.TextArea
                                placeholder='Nazwa ćwiczenia'
                                onChange={handleExerciseNameChange}
                                value={userInput.name}
                            />
                            <h4>Opis ćwiczenia</h4>
                            <Form.TextArea
                                placeholder='Opis ćwiczenia'
                                onChange={handleExerciseDescriptionChange}
                                value={userInput.description}
                            />
                            <div style={{
                                padding: '0 0 10px 0',
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'end',
                                justifyContent: 'space-between'
                            }}>
                                <h4>Tagi</h4>
                                <Card.Group>
                                    {userInput.tags != null ? userInput.tags.map((tag) => (
                                        <TagElement name={tag}/>
                                    )) : null}
                                </Card.Group>
                                <Button color={"blue"} onClick={handleHashtagsChange}>Dodaj tag</Button>
                            </div>
                            <Form.TextArea
                                placeholder='Tagi'
                                onChange={handleTagChange}
                                value={tag}
                            />
                            <h4>Liczba serii</h4>
                            <Input
                                placeholder='Ilość serii'
                                type='number'
                                onChange={handleNumberOfSeriesChange}
                                value={userInput.numberOfSeries}>
                            </Input>
                            <h4>Kroki ćwiczenia</h4>
                            <Grid.Row>
                                <Input
                                    placeholder='Długość w sekundach'
                                    type='number'
                                    onChange={handleStepTimeChange}
                                    value={step.time}>
                                </Input>
                                <Select placeholder='Rodzaj akcji' options={questionTypeOptions}
                                        onChange={handleStepActionChange}/>
                                <Dropdown placeholder='Parametr' clearable options={parameterOptions} selection  onChange={handleMeasurementParameterTypeChange}/>
                                <Button color="blue" onClick={handleExerciseStepsChange}>
                                    Dodaj
                                </Button>
                            </Grid.Row>
                            <br/>
                            <ExerciseStepsContainer questionList={userInput.steps} deleteFunction={deleteFunction}/>
                            <br/>
                            <Button color="blue" onClick={handleAddExercise}>
                                Dodaj ćwiczenie
                            </Button>
                        </Grid.Column>
                    </div>
                </Grid>
            </div>
        </HomePageInnerContainer>
    )
}

export default HomePageForm