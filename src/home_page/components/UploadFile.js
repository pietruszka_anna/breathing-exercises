import React, { useState } from "react";

export function Upload(props) {
    const [exercises, setExercises] = useState({
        exercises: []
    });

    const parseJsonExercises = (data) => {
        var exercise;
        var exercisesList = exercises.exercises;
        data.exercises.forEach((element) => {
            exercise = {
                name: element.name,
                description: element.description,
                numberOfSeries: element.numberOfSeries,
                steps: element.steps,
                tags: element.tags,
            }
            exercisesList.push(exercise)
        })
        setExercises(prevState => ({
            ...prevState,
            exercises: exercisesList
        }));
        props.loadExercises(exercises)
    }

    const handleChange = e => {
        const fileReader = new FileReader();
        fileReader.readAsText(e.target.files[0], "UTF-8");
        fileReader.onload = e => {
            parseJsonExercises(JSON.parse(e.target.result))
        };
    };
    return (
            <input type="file" onChange={handleChange} />
    );
}