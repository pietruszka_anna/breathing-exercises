import {Card} from "semantic-ui-react";
import ExerciseElement from "../../exercises/ExerciseElement";

export default function HomePageExercisesList(props) {
    return (
        <div style={{padding: '5rem 5rem 5rem 5rem'}}>
            <Card.Group>
                {props.exercises != null ? props.exercises.exercises.map((ex, index) => (
                    <ExerciseElement index={index} name={ex.name} description={ex.description} tags={ex.tags}
                                     steps={ex.steps} deleteExerciseFunction={props.deleteExerciseFunction} numberOfSeries={ex.numberOfSeries} editFunction={props.editFunction} />
                )) : null}
            </Card.Group>
        </div>
    )
}